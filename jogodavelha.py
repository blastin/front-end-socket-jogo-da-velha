# -*- coding: utf-8 -*-

import tkinter as tk
from tkinter import messagebox
from clienteSocket import ClienteSocket


class JogoDaVelha:
    botoes = []
    cliente = None

    def __init__(self):
        self.VELHA = 0
        self.PERDI = -1
        self.GANHEI = 1
        self.janela = tk.Tk()
        self.cliente = ClienteSocket()
        self.status = None

    def iniciarSistema(self):
        self.definir_eventos_de_Botoes()
        self.janela.mainloop()

    def definir_eventos_de_Botoes(self):
        self.botoes.append(
            tk.Button(self.janela, text="   ", command=lambda: self.marcar_jogada(0)))
        self.botoes[0].grid(row=2, column=1)

        self.botoes.append(
            tk.Button(self.janela, text="   ", command=lambda: self.marcar_jogada(1)))
        self.botoes[1].grid(row=2, column=2)

        self.botoes.append(
            tk.Button(self.janela, text="   ", command=lambda: self.marcar_jogada(2)))
        self.botoes[2].grid(row=2, column=3)

        self.botoes.append(
            tk.Button(self.janela, text="   ", command=lambda: self.marcar_jogada(3)))
        self.botoes[3].grid(row=3, column=1)

        self.botoes.append(
            tk.Button(self.janela, text="   ", command=lambda: self.marcar_jogada(4)))
        self.botoes[4].grid(row=3, column=2)

        self.botoes.append(
            tk.Button(self.janela, text="   ", command=lambda: self.marcar_jogada(5)))
        self.botoes[5].grid(row=3, column=3)

        self.botoes.append(
            tk.Button(self.janela, text="   ", command=lambda: self.marcar_jogada(6)))
        self.botoes[6].grid(row=4, column=1)

        self.botoes.append(
            tk.Button(self.janela, text="   ", command=lambda: self.marcar_jogada(7)))
        self.botoes[7].grid(row=4, column=2)

        self.botoes.append(
            tk.Button(self.janela, text="   ", command=lambda: self.marcar_jogada(8)))
        self.botoes[8].grid(row=4, column=3)

        iniciar = tk.Button(self.janela, text="Iniciar", command=self.iniciar_jogo)
        iniciar.grid(row=3, column=4)

        def resetar():
            for botao in self.botoes:
                botao.configure(text="   ")
                botao.configure(state="normal")
            self.cliente.reconectar()
            self.iniciar_jogo()
            
        reset = tk.Button(self.janela, text="Resetar", command=resetar)
        reset.grid(row=4, column=4)

    def marcar_jogada(self, jogada):

        if self.status is None:

            if self.botoes[jogada].cget('text') == '   ':
                self.botoes[jogada].configure(text='X')
                self.botoes[jogada].configure(state="disable")
                if self.cliente.enviar_jogada(jogada) != self.cliente.CLIENTE_ENVIO_COM_SUCESSO:
                    print("Não é sua vez")
                else:
                    print("realizei jogada")

                self.status = self.verificar()

                if self.status is None:
                    self.receber_jogada_adversaria()
                    print("recebi jogada")
                    self.status = self.verificar()
                    print("status {}", self.status)

            else:
                messagebox.showwarning("Atenção!", "Já subescrito!")

    def receber_jogada_adversaria(self):
        jogada = self.cliente.receber_jogada()
        self.botoes[jogada].configure(text='O')
        self.botoes[jogada].configure(state="disable")

    def verificar(self):

        def acertou(x_or_o):

            status = None

            if x_or_o == "X":
                messagebox.showwarning("Otimo!", "Ganhei")
                status = self.GANHEI
            elif x_or_o == "O":
                messagebox.showwarning("Pena!", "Perdi")
                status = self.PERDI
            elif x_or_o == "velha":
                messagebox.showerror("Erro", "Deu velha!")
                status = self.VELHA

            for botao in self.botoes:
                botao.configure(state="disable")

            return status

        a1 = self.botoes[0].cget("text")
        a2 = self.botoes[1].cget("text")
        a3 = self.botoes[2].cget("text")
        b1 = self.botoes[3].cget("text")
        b2 = self.botoes[4].cget("text")
        b3 = self.botoes[5].cget("text")
        c1 = self.botoes[6].cget("text")
        c2 = self.botoes[7].cget("text")
        c3 = self.botoes[8].cget("text")

        retorno = None

        if a1 == a2 == a3 == "X" or a1 == a2 == a3 == "O":
            if a1 == a2 == a3 == "X":
                retorno = acertou("X")
            elif a1 == a2 == a3 == "O":
                retorno = acertou("O")

        elif b1 == b2 == b3 == "X" or b1 == b2 == b3 == "O":
            if b1 == b2 == b3 == "X":
                retorno = acertou("X")
            elif b1 == b2 == b3 == "O":
                retorno = acertou("O")

        elif c1 == c2 == c3 == "X" or c1 == c2 == c3 == "O":
            if c1 == c2 == c3 == "X":
                retorno = acertou("X")
            elif c1 == c2 == c3 == "O":
                retorno = acertou("O")

        elif a1 == b1 == c1 == "X" or a1 == b1 == c1 == "O":
            if a1 == b1 == c1 == "X":
                retorno = acertou("X")
            elif a1 == b1 == c1 == "O":
                retorno = acertou("O")

        elif a2 == b2 == c2 == "X" or a2 == b2 == c2 == "O":
            if a2 == b2 == c2 == "X":
                retorno = acertou("X")
            elif a2 == b2 == c2 == "O":
                retorno = acertou("O")

        elif a3 == b3 == c3 == "X" or a3 == b3 == c3 == "O":
            if a3 == b3 == c3 == "X":
                retorno = acertou("X")
            elif a3 == b3 == c3 == "O":
                retorno = acertou("O")

        elif a1 == b2 == c3 == "X" or a1 == b2 == c3 == "O":
            if a1 == b2 == c3 == "X":
                retorno = acertou("X")
            elif a1 == b2 == c3 == "O":
                retorno = acertou("O")

        elif a3 == b2 == c1 == "X" or a3 == b2 == c1 == "O":
            if a3 == b2 == c1 == "X":
                retorno = acertou("X")
            elif a3 == b2 == c1 == "O":
                retorno = acertou("O")

        testes = [a1, a2, a3, b1, b2, b3, c1, c2, c3]
        velha = 0
        for i in testes:
            if i != "   ":
                velha += 1
        if velha == 9:
            retorno = acertou("velha")

        return retorno

    def iniciar_jogo(self):
        print("iniciando jogo")
        if not self.cliente.inicio_jogada():
            self.receber_jogada_adversaria()
