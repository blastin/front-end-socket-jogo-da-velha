# -*- coding: utf-8 -*-

from socket import SOCK_STREAM, socket, AF_INET
import json


class ClienteSocket:
    TCP_IP = '127.0.0.1'
    TCP_PORT = 3001
    BUFFER_SIZE = 2 << 7
    conexao = None
    protocolo = None
    CLIENTE_SALA_NAO_LOTADA = 5004
    CLIENTE_ENVIO_COM_SUCESSO = 5005

    def __init__(self):
        self.conexao = socket(AF_INET, SOCK_STREAM)
        self.conexao.connect((self.TCP_IP, self.TCP_PORT))
        mensagem = self.conexao.recv(self.BUFFER_SIZE)
        self.protocolo = json.loads(mensagem.decode('utf-8'))

    def enviar_jogada(self, jogada):
        self.protocolo["camadaMensagem"]["jogada"] = jogada
        self.conexao.send(json.dumps(self.protocolo).encode('utf-8'))
        mensagem = self.conexao.recv(self.BUFFER_SIZE)
        self.protocolo = json.loads(mensagem.decode('utf-8'))
        return self.protocolo["status"]

    def inicio_jogada(self):
        return self.protocolo["camadaMensagem"]["clienteIniciaComunicacao"]

    def receber_jogada(self):
        mensagem = self.conexao.recv(self.BUFFER_SIZE)
        self.protocolo = json.loads(mensagem.decode('utf-8'))
        return self.protocolo["camadaMensagem"]["jogada"]

    def desconectar(self):
        self.conexao.close()

    def reconectar(self):
        self.desconectar()
        self.conexao = socket(AF_INET, SOCK_STREAM)
        self.conexao.connect((self.TCP_IP, self.TCP_PORT))
        mensagem = self.conexao.recv(self.BUFFER_SIZE)
        self.protocolo = json.loads(mensagem.decode('utf-8'))
